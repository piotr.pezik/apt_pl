"""
http://www.nltk.org/_modules/nltk/tag/perceptron.html
"""

from ap_tagger import PerceptronTagger
import logging
import evaluation
from data_handler import read_data
import data_handler
import smtplib
from email.mime.text import MIMEText
from __init__ import logger


def main():
    model_file = 'models/555.pickle'
    bclusters = data_handler.read_brown_clusters('./data/bclusts.tsv')
    test_sentences = 1000
    only_main_pos = False
    gold_data = './data/train-gold.ud.xml'
    stand_off_test_set = './data/gold-task-a-b.ud.xml'
    iterations = 5
    limit = -1 #2000

    logger.info("Reading test data.")

    # random.shuffle(TRAIN_SET)
    # TEST_SET = TRAIN_SET[-test_sentences:]
    # TRAIN_SET = TRAIN_SET[:-test_sentences]
    TEST_SET = read_data(stand_off_test_set, main_pos=only_main_pos)

    logger.info("Reading train data.")
    TRAIN_SET = read_data(gold_data,main_pos=only_main_pos,limit=limit)

    #https://clarin-pl.eu/dspace/handle/11321/270
    #https://clarin-pl.eu/dspace/bitstream/handle/11321/270/kpwr-1.2-disamb-rc1.7z?sequence=2&isAllowed=y

    #data_handler.add_kpwr(kpwr_docs_dir='/Users/piotrpezik/Downloads/kpwr-1.2-disamb-rc1/documents/', DATA=TRAIN_SET)

    logger.info("Read {} training sentences with {} words.".format(len(TRAIN_SET['sentences']),TRAIN_SET['tokens']))



    tagger = PerceptronTagger(load=False)
    logger.info('Training...')
    tagger.train(TRAIN_SET['sentences'], save_loc=model_file,
                 nr_iter=iterations,
                 bclusters=bclusters)


    eval = evaluation.evaluate_tagger(tagger, test_set=TEST_SET['sentences'],
                                      dict_tags=TEST_SET['dict_tags'],igns=TEST_SET['igns'],
                                      bclusters=bclusters)

    logger.info('Accuracy: {} overall. {} for {} igns.'.format(eval.accuracy, eval.ign_accuracy, eval.ign_samples))
    logger.info('Time: {} ({} words per sec). Total words: {}.'.format(eval.time, eval.num_samples / eval.time,
                                                                       eval.num_samples))

main()