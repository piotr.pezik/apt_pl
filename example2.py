#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ap_tagger import PerceptronTagger
import data_handler
import morfeusz2

model_file = 'models/20its.pickle'
bclusters = data_handler.read_brown_clusters('./data/bclusts.tsv')
phrase = [u'Tweetnij', u'tę', u'focię', '.']

tagger = PerceptronTagger(load=False)
print("Loading the model")
tagger.load(loc=model_file)

morfeusz = morfeusz2.Morfeusz(aggl='permissive', praet='composite')
dict_tags = []
for w in phrase:
	if morfeusz.analyse(w)[0][2][2] == 'ign':
		dict_tags.append('')
	else:
		dict_tags.append(morfeusz.analyse(w)[0][2][2])

for w in tagger.tag(tokens=phrase, dict_tags=dict_tags ,bclusters=bclusters):
	print("{}\t{}".format(w[0].encode('utf-8'), w[1].encode('utf-8')))

"""
Tweetnij	impt:sg:sec:perf
tę	adj:sg:acc:f:pos
focię	subst:sg:acc:f
.	interp
"""

