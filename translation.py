# -*- coding: utf-8 -*-
from enum import Enum

D_FIELD=Enum('D_FIELD', 'flexemes cats special_lemmas special_words default POS FEATURES')


nkjp_to_ud_dict={
    D_FIELD.flexemes.name:{
        'ger': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'NOUN',
                D_FIELD.FEATURES.name:[
                    ('VerbForm', 'Ger')
                ]
            }
        },
        'subst': {
            D_FIELD.special_lemmas.name: [
                (
                    ['kto', 'co'],  # Kto i co są w subst-ach
                    {
                        D_FIELD.POS.name: 'PRON',
                        D_FIELD.FEATURES.name: [('PronType', 'Int,Rel')]
                    }),
                (
                    ['coś', 'ktoś', 'cokolwiek', 'ktokolwiek'],
                    {
                        D_FIELD.POS.name: 'PRON',
                        D_FIELD.FEATURES.name: [('PronType', 'Ind')]
                    }),
                (
                    ['nikt', 'nic'],  # nikt i nic to subst
                    {
                        D_FIELD.POS.name: 'PRON',
                        D_FIELD.FEATURES.name: [('PronType', 'Neg')]
                    }),
                (
                    ['wszystko', 'wszyscy'],
                    {
                        D_FIELD.POS.name: 'PRON',
                        D_FIELD.FEATURES.name: [('PronType', 'Tot')]
                    }),
                (
                    ['to'],
                    {
                        D_FIELD.POS.name: 'PRON',
                        D_FIELD.FEATURES.name: [('PronType', 'Dem')]
                    })
            ],
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'NOUN',
                # D_FIELD.FEATURES.name:
            }
        },
        'pred': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'VERB',
                # D_FIELD.FEATURES.name:
            }
        },
        'comp': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'SCONJ',
                # D_FIELD.FEATURES.name:
            }
        },
        'interp': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'PUNCT'
                # D_FIELD.FEATURES.name:
            }
        },
        'conj': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'CONJ',
                # D_FIELD.FEATURES.name:
            }
        },
        'adv': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'ADV',
                # D_FIELD.FEATURES.name:
            }
        },
        'aglt': {
            D_FIELD.special_lemmas.name: [
                            (
                                ['być'],  # Kto i co są w subst-ach
                                {
                                    D_FIELD.POS.name: 'AUX',
                                    D_FIELD.FEATURES.name: [
                                        ('Mood', 'Ind'),
                                        ('Tense', 'Pres'),
                                        ('VerbForm', 'Fin')
                                    ]
                                })],
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'AUX',
                D_FIELD.FEATURES.name: [
                    ('PronType', 'Prs'),
                    ('Reflex', 'Yes'),
                    ('Mood', 'Ind'),
                    ('Tense', 'Pres'),
                    ('VerbForm', 'Fin')
                ]}
        },
        'bedzie': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'AUX',
                # D_FIELD.FEATURES.name:
            },
            D_FIELD.special_words.name: [
                (
                    ['będą', 'będzie', 'będę', 'będziemy', 'będziesz'],
                    {
                        D_FIELD.POS.name: 'AUX',
                        D_FIELD.FEATURES.name: [
                                            ('Tense', 'Fut'),
                                            ('Mood', 'Ind'),
                                            ('VerbForm', 'Fin'),
                                        ]})],
        },
        'burk': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'NOUN',
                # D_FIELD.FEATURES.name:
            }
        },
        'depr': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'NOUN',
                # D_FIELD.FEATURES.name:
            }
        },
        'ign': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'X',
                # D_FIELD.FEATURES.name:
            }
        },
        'dig': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'NUM',
                # D_FIELD.FEATURES.name:
            }
        },
        'romandig': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'NUM',
                # D_FIELD.FEATURES.name:
            }
        },
        'siebie': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'PRON',
                D_FIELD.FEATURES.name: [
                    ('PronType', 'Prs'),
                    ('Reflex', 'Yes')
                ]
            }
        },
        'numcol': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'NUM',
                D_FIELD.FEATURES.name: [
                    ('NumType', 'Sets')
                ]
            }
        },
        'winien': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'ADJ'
            },
            D_FIELD.special_lemmas.name: [
                (
                    ['powinien'],
                    {
                        D_FIELD.POS.name: 'ADJ',
                    })
            ]
        },
        # 'adj':{
        #     D_FIELD.special_lemmas.name: [
        #         (
        #         ['jaki', 'jakiś', 'żaden', 'wszystek', 'niejaki', 'który', 'taki', 'niektóry', 'którykolwiek', 'któryś',
        #          'ten', 'jakikolwiek', 'tamten', 'każdy', 'wszelki', 'ów'],
        #         {
        #             D_FIELD.POS.name: 'DET',
        #             D_FIELD.FEATURES.name: [('PronType', 'Ind')]
        #         })
        #     ],
        #     D_FIELD.default.name:{
        #         D_FIELD.POS.name:'ADJ'
        #     }
        # },
        'xxx':{
            D_FIELD.default.name:{
                D_FIELD.POS.name:'X'
            }
        },
        'interj': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'INTJ'
            }
        },
        'adj':{
            D_FIELD.special_lemmas.name: [
                (
                [ 'wszystek','wszyscy','każdy', 'wszelki'],
                {
                    D_FIELD.POS.name: 'DET',
                    D_FIELD.FEATURES.name: [('PronType', 'Tot')]
                }),
                (
                ['jaki', 'który'], #Kto i co są w subst-ach
                {
                    D_FIELD.POS.name: 'DET',
                    D_FIELD.FEATURES.name: [('PronType', 'Int,Rel')]
                }),
                (
                ['to','ten','taki','tamten', 'ów'],
                {
                    D_FIELD.POS.name: 'DET',
                    D_FIELD.FEATURES.name: [('PronType', 'Dem')]
                }),
                (
                ['jakiś', 'kilka', 'kilkadziesiąt', 'kilkaset', 'niektóry', 'któryś', 'jakikolwiek', 'niejaki', 'którykolwiek'], #kilkanaście jest w num, coś/ktoś w subst
                {
                    D_FIELD.POS.name: 'DET',
                    D_FIELD.FEATURES.name: [('PronType', 'Ind')]
                }),
                (
                ['żaden'], #nikt i nic to subst
                {
                    D_FIELD.POS.name: 'DET',
                    D_FIELD.FEATURES.name: [('PronType', 'Neg')]
                })
            ],
            D_FIELD.default.name:{
                D_FIELD.POS.name:'ADJ'
            }
        },
        'adjc':{
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'ADJ'
            },
            D_FIELD.special_words.name: [
                (
                ['winien','gotów','pewien','ciekaw','wart','pełen','świadom','pewnien',
                 'godzien','łaskaw','znan','rad','wesół','zdrów'],
                {
                    D_FIELD.POS.name: 'ADJ'
                })
            ]
        },
        'qub': {
            D_FIELD.special_lemmas.name: [
                (
                ['się'],
                {
                    D_FIELD.POS.name: 'PRON',
                    D_FIELD.FEATURES.name: [('PronType', 'Prs'), ('Reflex', 'Yes')]
                })
            ],
            D_FIELD.special_words.name: [
                (
                    ['sie', 'sia'],
                    {
                        D_FIELD.POS.name: 'PRON',
                        D_FIELD.FEATURES.name: [('PronType', 'Prs'), ('Reflex', 'Yes'), ('Typo', 'Yes')]
                    }),
                (
                    ['by'],
                    {
                        D_FIELD.POS.name: 'AUX',
                        D_FIELD.FEATURES.name: [('VerbForm', 'Fin'), ('Mood', 'Cnd'), ('Aspect', 'Imp'),]
                    })],
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'PART'
            }
        },
        'adja':{
            D_FIELD.default.name:{
                D_FIELD.POS.name:'ADJ',
                D_FIELD.FEATURES.name:[
                    ('Hyph', 'Yes')
                ]
            }
        },
        'prep': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'ADP',
                D_FIELD.FEATURES.name: [
                    ('AdpType', 'Prep')
                ]
            }
        },
        'praet': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'VERB',
                D_FIELD.FEATURES.name: [
                    ('Tense', 'Past'),
                    ('VerbForm', 'Part'),
                    ('Voice', 'Act')
                ]
            }
        },
        'pact': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'VERB',
                D_FIELD.FEATURES.name: [
                    ('VerbForm', 'Part'),
                    ('Voice', 'Act'),
                    ('Tense', 'Pres')
                ]
            }
        },
        'pant': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'VERB',
                D_FIELD.FEATURES.name: [
                    ('Tense', 'Past'),
                    ('VerbForm', 'Trans')
                ]
            }
        },
        'pcon': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'VERB',
                D_FIELD.FEATURES.name: [
                    ('Tense', 'Pres'),
                    ('VerbForm', 'Trans')
                ]
            }
        },
        'ppas': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'VERB',
                D_FIELD.FEATURES.name: [
                    ('VerbForm', 'Part'),
                    ('Voice', 'Pass')
                ]
            }
        },
        'num': {
            D_FIELD.special_lemmas.name: [
                (
                    ['kilkanaście', 'kilka', 'kilkadziesiąt', 'kilkaset'],
                    {
                        D_FIELD.POS.name: 'DET',
                        D_FIELD.FEATURES.name: [('PronType', 'Ind'), ('NumType', 'Card')]
                    })
            ],
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'NUM',
                D_FIELD.FEATURES.name: [
                    # ('NumType', 'Sets')
                ]
            }
        },
        'brev': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'X',
                # D_FIELD.FEATURES.name: [
                #     ('Abbr', 'Yes')
                # ]
            }
        },
        'adjp': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'ADJ',
                D_FIELD.FEATURES.name: [
                    ('PrepCase', 'Pre')
                ]
            }
        },
        'fin': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'VERB',
                D_FIELD.FEATURES.name: {
                    ('VerbForm', 'Fin'),
                    ('Tense', 'Pres'),
                    ('Mood', 'Ind')
                }
            }
        },
        'ppron12': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'PRON',
                D_FIELD.FEATURES.name: {
                    ('PronType', 'Prs')
                }
            }
        },
        'ppron3': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'PRON',
                D_FIELD.FEATURES.name: {
                    ('PronType', 'Prs')
                }
            }
        },
        'inf': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'VERB',
                D_FIELD.FEATURES.name: {('VerbForm', 'Inf')}
            }
        },
        # 'num':{
        #     #D_FIELD.special_lemmas.name :None,
        #     #D_FIELD.special_words.name :None,
        #     D_FIELD.default.name:{
        #         D_FIELD.POS.name:'NUM'
        #         #,D_FIELD.FEATURES.name:
        #     }
        # },
        'impt': {
            D_FIELD.default.name: {
                D_FIELD.POS.name: 'VERB',
                D_FIELD.FEATURES.name: [
                    ('Mood', 'Imp'),
                    ('VerbForm', 'Fin')
                ]
            }
        },
        'imps':{
            D_FIELD.default.name:{
                D_FIELD.POS.name:'VERB',
                D_FIELD.FEATURES.name:[
                    ('Case','Nom'),
                    ('Gender', 'Neut'),
                    ('Negative', 'Pos'),
                    ('Number','Sing'),
                    ('VerbForm', 'Part'),
                    ('Voice','Pass')
                ]
            }
        },
    },
    D_FIELD.cats.name:{
        'pl':{
            D_FIELD.default.name:{
                D_FIELD.FEATURES.name:{('Number','Plur')}
            }
        },
        'pun': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: [
                    ('Abbr', 'Yes')
                ]
            }
        },
        'npun': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: [
                    ('Abbr', 'Yes')
                ]
            }
        },
        'acc':{
            D_FIELD.default.name:{
                D_FIELD.FEATURES.name:{('Case','Acc')}
            }
        },
        'nakc': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Variant', 'Short')}
            }
        },
        'voc': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Case', 'Voc')}
            }
        },
        # 'ppron12': {
        #     D_FIELD.default.name: {
        #         D_FIELD.FEATURES.name: {
        #             ('PronType', 'Prs')
        #         }
        #     }
        # },
        'm1': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {
                    ('Animacy', 'Hum'),
                    ('Gender', 'Masc')
                }
            }
        },
        'm2': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {
                    ('Animacy', 'Anim'),
                    ('Gender', 'Masc')
                }
            }
        },
        'm3': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {
                    ('Animacy', 'Inan'),
                    ('Gender', 'Masc')
                }
            }
        },
        'rec': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {
                }
            }
        },
        'nagl': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {
                }
            }
        },
        'agl': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {
                }
            }
        },
        'congr': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {
                }
            }
        },
        'praep': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: [
                    ('PrepCase', 'Pre')
                ]
            }
        },
        '_': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {
                }
            }
        },
        'aff': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Polarity', 'Pos')}
            }
        },
        'com': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Degree', 'Cmp')}
            }
        },
        # 'com': {
        #     D_FIELD.default.name: {
        #         D_FIELD.FEATURES.name: {}
        #     }
        # },
        'perf':{
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Aspect', 'Perf')}
            }
        },
        'imperf': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Aspect', 'Imp')}
            }
        },
        'sg': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Number', 'Sing')}
            }
        },
        'gen': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Case', 'Gen')}
            }
        },
        'nom': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Case', 'Nom')}
            }
        },
        'pos': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Degree', 'Pos')}
            }
        },
        'akc': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Variant', 'Long')}
            }
        },
        'f': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Gender', 'Fem')}
            }
        },
        'dat': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Case', 'Dat')}
            }
        },
        'inst': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Case', 'Ins')}
            }
        },
        'loc': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Case', 'Loc')}
            }
        },
        'neg': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Polarity', 'Neg')}
            }
        },
        'npraep': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('PrepCase', 'Npr')}
            }
        },
        'nwok': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Variant', 'Short')}
            }
        },
        'wok': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Variant', 'Long')}
            }
        },
        'vok': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Variant', 'Long')}
            }
        },
        'xxx': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Foreign', 'Yes')}
            }
        },
        'pri': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Person', '1')}
            }
        },
        'sec': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Person', '2')}
            }
        },
        'ter': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Person', '3')}
            }
        },
        'sup': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Degree', 'Sup')}
            }
        },
        'n': {
            D_FIELD.default.name: {
                D_FIELD.FEATURES.name: {('Gender', 'Neut')}
            }
        }
    }
}

def get_main_ud_pos(nkjp_tag):
    main_nkjp_tag = nkjp_tag.split(':')[0]
    try:
        return nkjp_to_ud_dict[D_FIELD.flexemes.name][main_nkjp_tag].get(D_FIELD.default.name)[D_FIELD.POS.name]
    except:
        return main_nkjp_tag
