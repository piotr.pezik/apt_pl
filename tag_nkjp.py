from ap_tagger import PerceptronTagger
import data_handler
from  __init__ import logger
import os
import codecs
import json

nkjp_folder = '/Volumes/MacintoshHD/data/nkjp/morf/'
tagged_nkjp_folder = '/Volumes/MacintoshHD/data/nkjp/tagged'

model_file = 'models/20its.pickle'
bclusters = data_handler.read_brown_clusters('./data/bclusts.tsv')

tagger = PerceptronTagger(load=False)
print("Loading the model")
tagger.load(loc=model_file)


nkjp_files = [file for file in os.listdir(nkjp_folder) if file.endswith(".jpl")]

logger.info("Found {} files to tag.".format(len(nkjp_files)))

words_tagged=0
lines_tagged=0

for fin,nkjp_file in enumerate(nkjp_files):
    logger.info("Tagging file {} ({} of {}).".format(nkjp_file,fin+1,len(nkjp_files)))
    with codecs.open(nkjp_folder + '/' + nkjp_file, 'r', "utf-8") as f:
        with codecs.open(tagged_nkjp_folder + '/' + nkjp_file, 'w', "utf-8") as of:
            for line in f:
                tokens = json.loads(line)
                forms = []
                dict_tags = []
                lemmas=[]
                spaces=[]
                for wi,w in enumerate(tokens):
                    forms.append(w['o'])
                    tags_lemmas = {}
                    lemmas.append([])
                    spaces.append(w['is'][0]['s'])
                    for b_t in w['is']:
                        if b_t['t']!='ign':
                            tags_lemmas[b_t['t']]=b_t['b']
                    lemmas[wi] = tags_lemmas
                    dict_tags.append(set(tags_lemmas.keys()))
                tagged = tagger.tag(tokens=forms,dict_tags=dict_tags,bclusters=bclusters)
                for ti,t in enumerate(tagged):
                    tokens[ti]['dt']=t[1]
                    tokens[ti]['s']=spaces[ti]
                    if t[1] in lemmas[ti]:
                        tokens[ti]['l'] = lemmas[ti][t[1]]
                    else:
                        tokens[ti]['l'] = tokens[ti]['o']
                    del[tokens[ti]['is']]
                words_tagged+=len(tokens)
                lines_tagged+=1
                of.write(json.dumps(tokens).replace('\n',' ')+'\n')
                if (lines_tagged % 1000 == 0): logger.info("Total lines tagged so far: {}. Words: {}.".format(lines_tagged, words_tagged))

