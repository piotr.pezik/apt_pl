import ap_tagger
from ap_tagger import PerceptronTagger
import xml.etree.ElementTree as ET
import logging
import random
import evaluation
from data_handler import read_data
import data_handler
import sys
from collections import defaultdict

from  __init__ import logger


def main():
    #model_file = 'models/main_pos_5its.pickle'
    model_file = 'models/20its.pickle'
    bclusters = data_handler.read_brown_clusters('./data/bclusts.tsv')

    tagger = PerceptronTagger(load=False)
    logger.info("Loading the model")
    tagger.load(loc=model_file)

    for w in tagger.tag(['Tweetnij', 'tę', 'focię', '.'],bclusters=bclusters):
         print("{}\t{}".format(w[0], w[1]))

    # gold_data = './data/train-gold.xml'
    stand_off_test_set = './data/gold-task-a-b.xml'

    logger.info("Reading the gold data.")
    #TEST_SET = read_data(stand_off_test_set,main_pos=True)
    TEST_SET = read_data(stand_off_test_set,main_pos=False)
    #TEST_SET = {'sentences': [], 'dict_tags': [], 'igns': defaultdict(set), 'tokens': 0}

    #data_handler.add_kpwr(kpwr_docs_dir='/Users/piotrpezik/Downloads/kpwr-1.2-disamb-rc1/documents/', DATA=TEST_SET)
    #TEST_SET['dict_tags'] = None

    logger.info('Evaluating accuracy on {} sentences.'.format(len(TEST_SET['sentences'])))

    eval = evaluation.evaluate_tagger(tagger,
                                      test_set=TEST_SET['sentences'],
                                      dict_tags=TEST_SET['dict_tags'],
                                      igns=TEST_SET['igns'], main_pos=False, bclusters=bclusters)

    logger.info('Accuracy: {} overall. {} for {} igns.'.format(eval.accuracy, eval.ign_accuracy, eval.ign_samples))
    logger.info('Time: {} ({} words per sec). Total words: {}.'.format(eval.time, eval.num_samples/eval.time, eval.num_samples))


main()
