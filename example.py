#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ap_tagger import PerceptronTagger
import data_handler

model_file = 'models/20its.pickle'
bclusters = data_handler.read_brown_clusters('./data/bclusts.tsv')

tagger = PerceptronTagger(load=False)
print("Loading the model")
tagger.load(loc=model_file)

for w in tagger.tag([u'Tweetnij', u'tę', u'focię', '.'],bclusters=bclusters):
        print(u"{}\t{}".format(w[0], w[1]))

"""
Tweetnij	impt:sg:sec:perf
tę	adj:sg:acc:f:pos
focię	subst:sg:acc:f
.	interp
"""
