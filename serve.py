import cherrypy
import json
from ap_tagger import PerceptronTagger
import data_handler
import random
import analyzer
from __init__ import logger
import sys
import translation



model_file = 'models/20its.pickle'
bclusters = data_handler.read_brown_clusters('./data/bclusts.tsv')


taggers = {}

num_taggers = 1
if(len(taggers)==0):
    for x in range(num_taggers):
        tagger = PerceptronTagger(load=False)
        print("Loading tagger {} of {}.".format(x+1,len(range(num_taggers))))
        tagger.load(loc=model_file)
        taggers[x]=tagger


def tag(tokens, dict_tags, bclusters=bclusters):
    tagger_key = random.choice(taggers.keys())
    tagger = taggers[tagger_key]
    #logger.info("T {}.".format(tagger_key))
    tagged = tagger.tag(tokens=tokens, dict_tags=dict_tags, bclusters=bclusters)
    return tagged


class TaggerService(object):
    @cherrypy.expose
    def index(self):
        logger.info("Starting the tagger.")
        return "APT_PL Tagger"

    @cherrypy.expose
    @cherrypy.tools.json_out()
    @cherrypy.tools.json_in()
    def tag_sentences(self, sentences):
        """
        :param text:
        :return:
         http://127.0.0.1:XXXX/tag_analyzed?sentences=.
        """
        words_tagged = 0
        sents = json.loads(sentences)
        analyzed_sentences = [analyzer.analyze_text(s) for s in sents]
        for sentence in analyzed_sentences:
            forms = []
            dict_tags = []
            lemmas = []
            names = []
            spaces = []
            for wi, w in enumerate(sentence):
                forms.append(w['o'])
                tags_lemmas = {}
                tags_names = {}
                lemmas.append([])
                names.append([])
                spaces.append(w['is'][0]['s'])
                for b_t in w['is']:
                    if b_t['t'] != 'ign':
                        tags_lemmas[b_t['t']] = b_t['b']
                        tags_names[b_t['t']]=b_t['n']
                lemmas[wi] = tags_lemmas
                names[wi] = tags_names
                dict_tags.append(set(tags_lemmas.keys()))
            tagged = tag(tokens=forms, dict_tags=dict_tags, bclusters=bclusters)
            words_tagged = words_tagged + len(tagged)
            for ti, t in enumerate(tagged):
                sentence[ti]['dt'] = t[1]
                sentence[ti]['udt'] =  translation.get_main_ud_pos(t[1])

                sentence[ti]['s'] = spaces[ti]
                if t[1] in lemmas[ti]:
                    sentence[ti]['l'] = lemmas[ti][t[1]]
                    if len(names[ti][t[1]])>0: sentence[ti]['n'] = names[ti][t[1]]
                else:
                    sentence[ti]['l'] = sentence[ti]['o']
                del [sentence[ti]['is']]
        logger.info("{}w {}s.".format(words_tagged, len(analyzed_sentences)))
        return {"sentences":analyzed_sentences}

    # @cherrypy.expose
    # @cherrypy.tools.json_out()
    # @cherrypy.tools.json_in()
    # def tag_tokenized_sentences(self, sentences):
    #     pass

if __name__ == '__main__':
    logger.info("Usage: python serve.py [HOST] [PORT]")
    host = sys.argv[1]
    port = int(sys.argv[2])
    logger.info("Host: {} Port: {}".format(host,port))
    cherrypy.config.update({'log.screen': False,
                            # 'log.access_file': './logs/tagger_access.log',
                            # 'log.error_file': './logs/tagger_error.log',
                            'server.socket_port': port, 'server.socket_host': host,
                            'tools.encode.encoding': "utf-8",
                            'tools.json_in.on': True,
                            'tools.json_in.force': False
                            })

    cherrypy.engine.unsubscribe('graceful', cherrypy.log.reopen_files)
    cherrypy.quickstart(TaggerService())
