import glob, os
import xml.etree.ElementTree as ET
import logging
from collections import defaultdict
import codecs

logger = logging.getLogger('tagger')


def read_data(input, limit = -1, main_pos =False, skip_tags_for_igns=True):

    DATA = {'sentences':[],'dict_tags':[], 'igns':defaultdict(set),'tokens':0}
    root = ET.parse(input).getroot()
    #for s_chunk in root.findall(".//chunk[@type='s']|.//sentence"):
    for s_chunk in (root.findall(".//chunk[@type='s']") + root.findall(".//sentence")):
        #words,lemmas,tags,=[],[],[]
        sent = []
        all_tags_sent = []
        if len(DATA['sentences'])> 0 and len(DATA['sentences'])%10000==0: logger.info("Preparing sentence: {}.".format(len(DATA['sentences'])))
        if limit !=-1 and len(DATA['sentences']) >=limit: break
        for toki, tok in enumerate(s_chunk.findall(".//tok")):
            DATA['tokens']+=1
            form = tok.find("./orth").text
            #words.append(form if len(form)>0 else '_')
            #lemma = tok.find("./lex/base").text
            #lemmas.append(lemma if len(lemma)>0 else '_')
            all_tags = set([])
            if main_pos:
                xpostag = tok.find("./lex[@disamb='1']/ctag").text.split(':')[0]
                for at in tok.findall("./lex/ctag"):
                    all_tags.add(at.text.split(':')[0])
            else:
                xpostag = tok.find("./lex[@disamb='1']/ctag").text
                for at in tok.findall("./lex/ctag"):
                    all_tags.add(at.text)
            if ('ign' in all_tags or 'X' in all_tags):
                DATA['igns'][len(DATA['sentences'])].add(toki)
                if skip_tags_for_igns:
                    all_tags.clear()
            sent.append((form,xpostag))
            all_tags_sent.append(all_tags)
        DATA['sentences'].append(sent)
        DATA['dict_tags'].append(all_tags_sent)

    return(DATA)

def add_kpwr(kpwr_docs_dir,DATA,main_pos=False,skip_tags_for_igns=True):
    owd = os.getcwd()
    os.chdir(kpwr_docs_dir)
    fi=0
    for fn in glob.glob("*.xml"):
        if not os.path.basename(fn).endswith('.rel.xml'):
            fi += 1
            doc_data = read_data(fn,main_pos=main_pos,skip_tags_for_igns=skip_tags_for_igns)
            DATA['sentences'].extend(doc_data['sentences'])
            DATA['dict_tags'].extend(doc_data['dict_tags'])
            DATA['tokens'] = DATA['tokens']  + doc_data['tokens']
            if fi % 100 == 0: logger.info("Processed {} KPWr files.".format(fi))
    os.chdir(owd)

def read_brown_clusters(bc_file):
    bcs = {}
    with codecs.open(bc_file, 'r', "utf-8") as f:
        for line in f:
            fields = line.strip().split('\t')
            bcs[fields[1]]=fields[0]
    return bcs

