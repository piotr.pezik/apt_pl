from ap_tagger import PerceptronTagger
import time
import logging
import codecs

logger = logging.getLogger('tagger')


class Evaluation:
    def __init__(self,accuracy,num_samples,ign_accuracy, ign_samples,time):
        self.num_samples = num_samples
        self.accuracy = accuracy
        self.time = time
        self.ign_samples = ign_samples
        self.ign_accuracy = ign_accuracy


def evaluate_tagger(full_tagger,test_set, dict_tags=None, igns={}, log_every=100, main_pos=False, bclusters=None):
    start = time.time()
    score = 0.
    num_samples = 0.

    ign_score = 0.
    ign_samples = 0.

    for gsi, gold_sent in enumerate(test_set):
        gold_words = [wt[0] for wt in gold_sent]
        if dict_tags:
            tagged_sent = full_tagger.tag(gold_words, dict_tags=dict_tags[gsi],bclusters=bclusters)
        else:
            tagged_sent = full_tagger.tag(gold_words,bclusters=bclusters)
        if(gsi>1 and gsi%log_every ==0): logger.info("Tested on {} of {}.".format(gsi,len(test_set)))
        for wi, w in enumerate(gold_sent):
            if main_pos:
                auto_tag = w[1].split(':')[0]
                gold_tag = tagged_sent[wi][1].split(':')[0]
            else:
                auto_tag = w[1]
                gold_tag = tagged_sent[wi][1]
            if (gsi in igns) and (igns[gsi] != None and wi in igns[gsi]):
                ign_samples += 1
                if gold_tag == auto_tag:
                    ign_score += 1
            num_samples += 1
            if auto_tag == gold_tag:
                score += 1

    end = time.time()
    logger.info("Tested on {} of {}.".format(gsi, len(test_set)))
    if ign_samples> 0:
        ign_accuracy = (ign_score / ign_samples)
    else:
        ign_accuracy = 0.
    return Evaluation(accuracy=score/num_samples,num_samples=num_samples, ign_samples=ign_samples,
                      ign_accuracy=ign_accuracy, time=end-start)