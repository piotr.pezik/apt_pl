# -*- coding: utf-8 -*-
import morfeusz2
import _morfeusz2


morfeusz = morfeusz2.Morfeusz(whitespace=_morfeusz2.KEEP_WHITESPACES)


def collectInner(a):
    if len(a) == 0:
        return []
    if len(a) == 1:
        return a[0]
    result = []
    for x in a[0]:
        for y in collectInner(a[1:]):
            result.append(x + ':' + y)
    return result

def unpackTag(tag_s):
    return collectInner([a.split('.') for a in tag_s.split(':')])


def analyze_text(sent_text):
    sentence = {}
    toks = morfeusz.analyse(unicode(sent_text))
    for toki, tok in enumerate(toks):
        toks_at_pos = sentence.get(tok[0], {})
        iss = toks_at_pos.get('is', [])
        toks_at_pos['o'] = tok[2][0]
        for unpacked_t in unpackTag(tok[2][2]):
            iss.append({'b': tok[2][1], 't': unpacked_t, 's': 0,'n':tok[2][3]})
        toks_at_pos['is'] = iss
        sentence[tok[0]] = toks_at_pos
    new_sentence = []
    for w in sentence:
        if sentence[w]['is'][0]['t'] == 'sp':
            if (w - 1) in sentence:
                for isi in range(len(sentence[w - 1]['is'])):
                    sentence[w - 1]['is'][isi]['s'] = 1
        else:
            new_sentence.append(sentence[w])
    return new_sentence


#print(analyze(u'Ala mieszka w Pile i ma kota.'))

